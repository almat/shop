<?php

namespace Database\Seeders;

use App\Models\Store;
use Illuminate\Database\Seeder;

class StoreSeeder extends Seeder
{
    protected $model = Store::class;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Store::upsert([
            [
                'name' => 'Москва',
                'description' => 'Склад в Москве',
                'latitude' => 55.878315,
                'longitude' => 37.65372,
                'address' => 'г Москва, ул Сухонская, д 11, кв 89',
            ],
            [
                'name' => 'Уфа',
                'description' => 'Склад в Уфе',
                'latitude' => 54.732136,
                'longitude' => 55.948772,
                'address' => 'г Уфа, ул Ленина, д 33/1',
            ],
            [
                'name' => 'Курган',
                'description' => 'Склад в Кургане',
                'latitude' => 55.444384,
                'longitude' => 65.295457,
                'address' => 'г Курган, ул А.Матросова, д 30',
            ],
        ], ['id']);
    }
}
